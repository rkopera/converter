<?php
App::import('Currencies.Model', 'CurrenciesAppModel');

/**
 * Class Currency
 */
class Currency extends CurrenciesAppModel {

	/**
	 * do not use db table
	 * @var bool
	 */
	public $useTable = false;

	/**
	 * currencies names constants
	 */
	const CURRENCY_PLN = 'PLN';
	const CURRENCY_RUB = 'RUB';

	/**
	 * validation rules
	 * @var array
	 */
	public $validate = array(
		'amount' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'Amount field cannot be empty!'
			),
			'numeric' => array(
				'rule' => 'numeric',
				'message' => 'Amount must be numeric!'
			),
			'bigger' => array(
				'rule' => array('comparison', '>', 0),
				'message' => 'Amount must be bigger than 0!'
			)
		)
	);

	/**
	 * beforeValidate
	 * @param array $options
	 * @return bool
	 */
	public function beforeValidate($options = array()) {
		if (isset($this->data[$this->alias]['amount'])) {
			$this->data[$this->alias]['amount'] = str_replace(',', '.', $this->data[$this->alias]['amount']);
		}
		return parent::beforeValidate($options);
	}
}