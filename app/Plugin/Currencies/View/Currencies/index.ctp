<div class="row">
	<div class="col-xs-offset-0 col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title"><i class="fa fa-refresh"></i> Converter</h1>
			</div>
			<div class="panel-body">
				<?php

				echo $this->Form->create('Currency', array(
					'inputDefaults' => array(
						'class' => 'form-control',
						'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
						'div' => array('class' => 'form-group'),
					)));

				echo $this->Form->hidden('Currency.currency_from', array('value' => $currencyFrom));
				echo $this->Form->hidden('Currency.currency_to', array('value' => $currencyTo));
				echo $this->Form->input('Currency.amount', array(
					'label' => $currencyFrom,
					'autofocus' => true,
					'placeholder' => 'Enter amount...'));

				echo $this->Js->submit('Convert to ' . $currencyTo, array(
					'htmlAttributes' => array(
						'class' => 'btn btn-primary w100',
						'data-loading-text' => 'Converting, please wait...',
						'id' => 'send',
					),
					'async' => true,
					'update' => '#result',
					'method' => 'post',
					'url' => array('action' => 'convert'),
					'complete' => '$("#result").show(); $("#send").button("reset")',
					'before' => '$("#result").hide(); $("#send").button("loading")',
				));
				echo $this->Form->end();
				echo $this->Js->writeBuffer();
				?>
				<div class="alert alert-success result" id="result"></div>
			</div>
		</div>
	</div>
</div>