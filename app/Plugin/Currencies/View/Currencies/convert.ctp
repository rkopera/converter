<?php
$currencyOptions = array(
    'wholePosition' => 'after',
    'decimals' => ',',
    'thousands' => ' ',
);
echo $this->Number->currency($amount, $from, $currencyOptions) . ' <i class="fa fa-exchange"></i> ' . $this->Number->currency($result, $to, $currencyOptions);