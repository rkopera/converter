<?php
App::import('Currencies.Model', 'Currency');

/**
 * Class ConvertController
 * @property    Currency $Currency
 * @property    ConvertApiComponent $ConvertApi
 */
class CurrenciesController extends AppController {

	/**
	 * Use Models
	 */
	public $uses = array('Currencies.Currency');

	/**
	 * Use Components
	 */
	public $components = array('Currencies.ConvertApi');

	/**
	 * index action
	 */
	public function index() {
		$this->set('currencyFrom', Currency::CURRENCY_RUB);
		$this->set('currencyTo', Currency::CURRENCY_PLN);
	}

	/**
	 * convert action for ajax
	 */
	public function convert() {
		if(!$this->request->is('post') || !$this->request->is('ajax')){
			throw new Exception('Bad request!');
		}
		$this->layout = 'ajax';
		//validate data
		$this->Currency->set($this->request->data);
		if (!$this->Currency->validates()) {
			return new CakeResponse(array('body' => $this->Currency->validationErrors['amount'][0], 'status' => 200));
		}
		//try convert amount
		$amount = $this->Currency->data['Currency']['amount'];
		$from = $this->Currency->data['Currency']['currency_from'];
		$to = $this->Currency->data['Currency']['currency_to'];
		if (!$result = $this->ConvertApi->convert($amount, $from, $to)) {
			return new CakeResponse(array('body' => 'We can\'t receive data from server!', 'status' => 200));
		}
		$this->set(compact('amount', 'from', 'to', 'result'));
	}

	private function __validateConvert(){

	}

}