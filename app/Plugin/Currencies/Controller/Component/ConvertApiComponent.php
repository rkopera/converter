<?php

/**
 * Class ConvertApiComponent
 */
class ConvertApiComponent extends Component {

	/**
	 * convert amount to currency
	 * @param $amount
	 * @param $from
	 * @param $to
	 * @return bool|mixed
	 */
	public function convert($amount, $from, $to) {
		if (!$Rate = $this->getRate($from, $to)) {
			return false;
		}
		return $amount * $Rate->query->results->rate->Rate;
	}

	/**
	 * get rate of currency
	 * @param $from
	 * @param $to
	 * @return bool|mixed
	 */
	public function getRate($from, $to) {
		$yahooApiUrl = 'https://query.yahooapis.com/v1/public/yql?q='
			. 'select%20*%20from%20yahoo.finance.xchange%20'
			. 'where%20pair%20in%20(%22' . $from . $to . '%22)&'
			. 'format=json&env=store%3A%2F%2Fdatatables.org%'
			. '2Falltableswithkeys&callback=';
		if (!$rate = @file_get_contents($yahooApiUrl)) {
			return false;
		}
		return json_decode($rate);
	}
}