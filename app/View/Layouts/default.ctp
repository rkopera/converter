<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'Convert' ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
	echo $this->Html->meta('icon');

	//jQuery
	echo $this->Html->script('jquery-2.1.4.min');

	//bootstrap
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->script('bootstrap.min');

	//font-awesome
	echo $this->Html->css('font-awesome.min');

	echo $this->Html->css('style');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body>
<div id="container">
	<div class="container">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>

	<footer>
		<div class="container">
			<hr>
			<h5>Created by <b>Robert Kopera</b></h5>
		</div>
	</footer>
</div>
</body>
</html>
